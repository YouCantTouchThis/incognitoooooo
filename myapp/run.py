
import numpy as np
import tensorflow as tf
import tensorflow_model_optimization as tfmot
from keras.optimizers import Adam
import pandas as pd
import sklearn.model_selection as skl
from keras.preprocessing.image import ImageDataGenerator
import tempfile
from keras.models import Sequential, Model
from keras.applications.vgg16 import VGG16
from keras.layers import GlobalAveragePooling2D, Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.models import Sequential, save_model, load_model
import os
import glob
from skimage import transform
from PIL import Image
path = []
for file in os.listdir("C:/Users/Sumeet/Desktop/Incognito/Incognito/media/images"):
    path.append(file)
print(path)
model = load_model('./pruned_model', compile=False)
loss = 'CategoricalCrossentropy'
optimizer = Adam(lr=1e-5)
metrics = ['binary_accuracy']
model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
def load(filename):
   np_image = Image.open("C:/Users/Sumeet/Desktop/Incognito/Incognito/media/images/" + filename)
   np_image = np.array(np_image).astype('float32')/255
   np_image = transform.resize(np_image, (244, 244, 3))
   np_image = np.expand_dims(np_image, axis=0)
   return np_image
new_image = load(path[0])
prediction = model.predict_classes(new_image)
classes = np.argmax(prediction, axis = -1)
print(classes)
