import numpy as np 
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
import sklearn.model_selection as skl
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import GlobalAveragePooling2D, Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.models import Sequential, Model
from keras.applications.resnet import ResNet50
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, EarlyStopping, ReduceLROnPlateau
from tensorflow.keras.models import Sequential, save_model, load_model
from skimage import transform
from keras.preprocessing import image
from PIL import Image
#import tensorflowjs as tfjs
from keras.applications.vgg16 import VGG16
import os

train_df = pd.read_csv('C:/Users/Sumeet/Desktop/train_df.csv')
test_df = pd.read_csv('C:/Users/Sumeet/Desktop/test_df.csv')
train_df, valid_df = skl.train_test_split(train_df, test_size = 0.2,)
train_idg = ImageDataGenerator(rescale=1. / 255.0,horizontal_flip = True, vertical_flip = False, height_shift_range= 0.1, width_shift_range=0.1, 
rotation_range=20, shear_range = 0.1,zoom_range=0.1)

train_gen = train_idg.flow_from_dataframe(dataframe=train_df, directory='C:/Users/Sumeet/Desktop/spiral/training', x_col = 'img_path', y_col ='class',class_mode = 'categorical',target_size = (224, 224), 
batch_size = 8)
val_idg = ImageDataGenerator(rescale=1. / 255.0)

val_gen = val_idg.flow_from_dataframe(dataframe=test_df,directory='C:/Users/Sumeet/Desktop/spiral/testing', x_col = 'img_path',y_col = 'class',class_mode = 'categorical',target_size = (224, 224), 
batch_size = 4)
valX, valY = val_gen.next()
model = VGG16(include_top=True, weights='imagenet')
transfer_layer = model.get_layer('block5_pool')
vgg_model = Model(inputs=model.input,outputs=transfer_layer.output)
for layer in vgg_model.layers[0:17]:
    layer.trainable = False
new_model = Sequential()
new_model.add(vgg_model)
new_model.add(Flatten())
new_model.add(Dense(1024, activation='relu'))
new_model.add(Dropout(0.2))
new_model.add(Dense(512, activation='relu'))
new_model.add(Dropout(0.2))
new_model.add(Dense(2, activation='softmax'))
new_model.add(tf.keras.layers.Reshape((-1,)))
checkpoint_path = "./small_model.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)
cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,save_weights_only=True,verbose=1)


optimizer = Adam(lr=1e-5)
loss = 'CategoricalCrossentropy'
metrics = ['CategoricalAccuracy']
print(new_model.summary())
new_model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
history = new_model.fit_generator(train_gen, validation_data = (valX, valY), epochs =2, steps_per_epoch=4, callbacks=[cp_callback])
filepath = './saved_model'
save_model(model, filepath, overwrite=True)