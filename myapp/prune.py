import numpy as np
import tensorflow as tf
import tensorflow_model_optimization as tfmot
from tensorflow_model_optimization.sparsity import keras as sparsity
from tensorflow_model_optimization.sparsity.keras import prune_low_magnitude
from keras.optimizers import Adam
import pandas as pd
import sklearn.model_selection as skl
from keras.preprocessing.image import ImageDataGenerator
import tempfile
from keras.models import Sequential, Model
from keras.applications.vgg16 import VGG16
from keras.layers import GlobalAveragePooling2D, Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.models import Sequential, save_model, load_model

train_df = pd.read_csv('C:/Users/Sumeet/Desktop/train_df.csv')
test_df = pd.read_csv('C:/Users/Sumeet/Desktop/test_df.csv')
train_df, valid_df = skl.train_test_split(train_df, test_size = 0.2,)
train_idg = ImageDataGenerator(rescale=1. / 255.0,horizontal_flip = True, vertical_flip = False, height_shift_range= 0.1, width_shift_range=0.1, 
rotation_range=20, shear_range = 0.1,zoom_range=0.1)

train_gen = train_idg.flow_from_dataframe(dataframe=train_df, directory='C:/Users/Sumeet/Desktop/spiral/training', x_col = 'img_path', y_col ='class',class_mode = 'categorical',target_size = (224, 224), 
batch_size = 16)
val_idg = ImageDataGenerator(rescale=1. / 255.0)

val_gen = val_idg.flow_from_dataframe(dataframe=test_df,directory='C:/Users/Sumeet/Desktop/spiral/testing', x_col = 'img_path',y_col = 'class',class_mode = 'categorical',target_size = (224, 224), 
batch_size = 8)
valX, valY = val_gen.next()

model = VGG16(include_top=True, weights='imagenet')
transfer_layer = model.get_layer('block5_pool')
vgg_model = Model(inputs=model.input,outputs=transfer_layer.output)
for layer in vgg_model.layers[0:17]:
    layer.trainable = False
new_model = Sequential()
new_model.add(vgg_model)
new_model.add(Flatten())
new_model.add(Dense(1024, activation='relu'))
new_model.add(Dropout(0.2))
new_model.add(Dense(512, activation='relu'))
new_model.add(Dropout(0.2))
new_model.add(Dense(2, activation='softmax'))
new_model.add(tf.keras.layers.Reshape((-1,)))
new_model.load_weights('./small_model.ckpt')
new_model.summary()
epochs = 2
end_step = np.ceil(1.0 * 72 / 8).astype(np.int32) * epochs
def apply_pruning_to_dense(layer):
    if isinstance(layer, tf.keras.layers.Dense):
        return tfmot.sparsity.keras.prune_low_magnitude(layer)
    return layer
model_for_pruning = tf.keras.models.clone_model(
    new_model,
    clone_function=apply_pruning_to_dense,
)

model_for_pruning.summary()

model_for_pruning.compile(loss=tf.keras.losses.categorical_crossentropy,optimizer = Adam(lr=1e-5),metrics = ['CategoricalAccuracy'])
callbacks = [sparsity.UpdatePruningStep(),sparsity.PruningSummaries(log_dir = tempfile.mkdtemp(), profile_batch=0)]
model_for_pruning.fit(train_gen,batch_size=8,epochs=5,verbose=1,callbacks=callbacks,validation_data=(valX, valY))
score = model_for_pruning.evaluate(valX, valY, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])
final_model = sparsity.strip_pruning(model_for_pruning)
final_model.summary()
filepath = './pruned_model'
save_model(final_model, filepath, overwrite=True)