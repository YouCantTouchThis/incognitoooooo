from django.shortcuts import render

from .forms import DiagnosisForm

from .models import Diagnosis
# Create your views here.


import numpy as np
import smtplib
import os
from email.message import EmailMessage
import ssl
from keras.optimizers import Adam
import sklearn.model_selection as skl
import tempfile
from keras.models import Sequential, Model
from keras.layers import GlobalAveragePooling2D, Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from tensorflow.keras.models import Sequential, save_model, load_model
import os
from skimage import transform
from PIL import Image
def diagnosis_create_view(request):
    form = DiagnosisForm(request.POST, request.FILES)
    if form.is_valid():
        print("VALID FORM \n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")
        form.save()
        path = []
        for file in os.listdir("C:/Users/Sumeet/Desktop/Incognito/Incognito/media/images"):
            path.append(file)
        print(path)
        model = load_model('C:/Users/Sumeet/Desktop/Incognito/Incognito/myapp/pruned_model', compile=False)
        loss = 'CategoricalCrossentropy'
        optimizer = Adam(lr=1e-5)
        metrics = ['binary_accuracy']
        model.compile(optimizer=optimizer, loss=loss, metrics=metrics)
        def load(filename):
            np_image = Image.open("C:/Users/Sumeet/Desktop/Incognito/Incognito/media/images/" + filename)
            np_image = np.array(np_image).astype('float32')/255
            np_image = transform.resize(np_image, (244, 244, 3))
            np_image = np.expand_dims(np_image, axis=0)
            return np_image
        new_image = load(path[0])
        prediction = model.predict_classes(new_image)
        classes = np.argmax(prediction, axis = -1)
        print(classes)
        server = smtplib.SMTP('smtp.gmail.com', 587)
        #Next, log in to the server
        context = ssl.create_default_context()
        server.starttls(context=context)
        #emailPassword = os.getenv('emailPassword')
        server.login("incognito.proccess@gmail.com", "NotCognito123!")

        #Send the mail
        msg = EmailMessage()
        if(classes == 0):
            msg.set_content("""
            Hey there!
            Incognito analyzed your email. We analyzed your image and found no signs of Parkinson's!
            Thanks,
            The Incognito Team
            """)
        else:
            msg.set_content("""
            Hey there!
            Incognito analyzed your email. We analyzed your image and detected Parkinson's. Please consult your doctor for further support.
            Thanks,
            The Incgonito Team
            """)

        email = form.cleaned_data['email']
        msg["Subject"] = "Incognito Parkinson's Results"
        msg["From"] = 'incognito.proccess@gmail.com'
        msg['To'] = email

        server.send_message(msg)
        server.quit()
    else:
        print("Invalid\n\n\n\n\n\n\n\n\n\n")
    context = {'form': form}
    return render(request, "diagnosis.html", context)